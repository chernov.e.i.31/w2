using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.DB;
using Microsoft.AspNetCore.Http;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private IDbRepo repo;

        public CustomerController(IDbRepo repo)
        {
            this.repo = repo;
        }

        [HttpGet("{id:long}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetCustomerAsync([FromRoute] long id)
        {
            var cust = repo.GetCustomerById(id).Result;
            if (cust != null)
            {
                return Ok(cust);
            }
            return BadRequest();
        }

        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customer)
        {
            var cust = repo.GetCustomerById(customer.Id).Result;
            if (cust == null)
            {
                return Ok(repo.AddCustomer(customer));
            }
            return Conflict();
        }
    }
}