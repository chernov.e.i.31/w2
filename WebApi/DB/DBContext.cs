﻿using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.DB
{
    public partial class DBContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {

        }
    }
}
