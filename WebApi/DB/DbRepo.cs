﻿using WebApi.Models;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DB
{
    public class DbRepo : IDbRepo
    {
        private DBContext _db;
        public DbRepo(DBContext Db)
        {
            _db = Db;
        }

        public long AddCustomer(Customer customer)
        {
            _db.Customers.Add(customer);
            _db.SaveChanges();
            return customer.Id;
        }

        public async Task<Customer> GetCustomerById(long Id)
        {
            return _db.Customers.Where(c => c.Id == Id).FirstOrDefault();
        }
    }
}
