﻿using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.DB
{
    public interface IDbRepo
    {
        Task<Customer> GetCustomerById(long Id);

        long AddCustomer(Customer customer);
    }
}
