﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        private static readonly HttpClient client = new ();
        static async Task Main(string[] args)
        {
            Console.WriteLine("1 - вывод пользователя по ID");
            Console.WriteLine("2 - добавить пользователя");
            Console.WriteLine("3 - выход");

            var exit = false;
            while (true)
            {
                Console.WriteLine("Выберите операцию:");
                switch (Console.ReadLine())
                {
                    case "1": Console.WriteLine(getCustomer().Result); break;
                    case "2": Console.WriteLine(RandomCustomer().Result); break;
                    case "3": exit = true; break;
                    default: break;
                }
                if (exit) break;
            }
        }

        private static async Task<string> RandomCustomer()
        {
            var fam = new string[10] { 
                "Кудряшов",
                "Давыдов",
                "Митрофанов",
                "Захаров",
                "Борисов",
                "Иванов",
                "Покровский",
                "Мешков",
                "Ефимов",
                "Трофимов"
            };

            var name = new string[10] {
                "Александр",
                "Даниил",
                "Лев",
                "Владимир",
                "Давид",
                "Кирилл",
                "Лев",
                "Артём",
                "Павел",
                "Егор"
            };

            Random random = new Random();
            var cust = new CustomerCreateRequest()
            {
                Firstname = name[random.Next(0, 9)],
                Lastname = fam[random.Next(0, 9)]
            };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(cust), Encoding.UTF8, "application/json");
            var result = await client.PostAsync("https://localhost:5001/customers", content);

            return await getCustomerById(int.Parse(await result.Content.ReadAsStringAsync()));
        }

        private static async Task<string> getCustomer()
        {
            Console.WriteLine("Введите ID");
            var str = Console.ReadLine();
            var result = "";
            if (int.TryParse(str, out int number))
            {
                result = await getCustomerById(number);
            }
            else
            {
                result = "введенное значение не является числом";
            }
            return result;
        }

        private static async Task<string> getCustomerById(int number)
        {
            HttpResponseMessage httpResponse = await client.GetAsync($"https://localhost:5001/customers/{number}");
            if (!httpResponse.IsSuccessStatusCode)
            {
                return "Пользователь не найден";
            }
            return await httpResponse.Content.ReadAsStringAsync();
        }
    }
}